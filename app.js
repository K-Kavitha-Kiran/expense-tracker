const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const fileUpload = require('express-fileupload');
const flash = require('connect-flash');

require('dotenv').config();

const port = process.env.PORT || 5000;

require('./server/models/database');

//express
const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));

//session & cookies
const session = require('express-session');
const cookieParser = require('cookie-parser');

app.use(cookieParser('Secret'));
app.use(session({
  secret: 'Secret',
  saveUninitialized: false,
  resave: true
}));

app.use(flash());
app.use(fileUpload());

app.use(expressLayouts);
app.set('layout', './layouts/main');
app.set('view engine', 'ejs');


const authRoutes = require('./server/routes/authRoutes');
app.use(authRoutes);

const expenseRoutes = require('./server/routes/expenseRoutes.js')
app.use('/', expenseRoutes);

app.listen(port, ()=> console.log('Listening to port '+ port));