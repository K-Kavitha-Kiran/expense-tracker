require('../models/database');
const Category = require('../models/Category');
const Expense = require('../models/Expense');


exports.homepage = async(req, res) =>{
    limitNumber = 5;
    const categories = await Category.find({}).limit(limitNumber);
  
    var categoryNames = new Array(categories.length);
    categories.forEach(categoriesToCategoryNames)
      function categoriesToCategoryNames(category, index) {
        categoryNames[index] = category.name;
      }
      
    var expenditure = new Array(categories.length);
    expenditure[0] = await Expense.find({ 'category': categoryNames[0] }).limit(limitNumber).where('user').equals(req.user.email);
    expenditure[1] = await Expense.find({ 'category': categoryNames[1] }).limit(limitNumber).where('user').equals(req.user.email);
    expenditure[2] = await Expense.find({ 'category': categoryNames[2] }).limit(limitNumber).where('user').equals(req.user.email);
    expenditure[3] = await Expense.find({ 'category': categoryNames[3] }).limit(limitNumber).where('user').equals(req.user.email);
    expenditure[4] = await Expense.find({ 'category': categoryNames[4] }).limit(limitNumber).where('user').equals(req.user.email);
    
    res.render("index", { title: 'Expenses Report', categories, expenditure, user: req.user});
}

exports.recentExpenses = async(req, res) =>{
    limitNumber = 20;
    const latest = await Expense.find({}).sort({date: -1}).limit(limitNumber).where('user').equals(req.user.email);
    const expenditure = latest;
    const mainHeading = "Latest Expenses";
    const descriptionPara = "Know what you have spent on recently.";
    res.render("display-expenses", { title: 'Recent Expenses', mainHeading, descriptionPara, expenditure, user: req.user});
}

exports.highExpenses = async(req, res) =>{
    limitNumber = 20;
    const highest = await Expense.find({}).sort({amount: -1}).limit(limitNumber).where('user').equals(req.user.email);
    const expenditure = highest;
    const mainHeading = "Huge Expenses";
    const descriptionPara = "Know where you have spent more.";
    res.render("display-expenses", { title: 'Recent Expenses', mainHeading, descriptionPara, expenditure, user: req.user});
}

exports.categoricalExpenses = async(req, res) =>{
    const categories = await Category.find({})
    res.render("categories", { title: 'Expenses - Category wise', categories, user: req.user});
}

exports.viewExpense = async(req, res) => {
    try {
      let expenseId = req.params.id;
      const expense = await Expense.findById(expenseId).where('user').equals(req.user.email);
      res.render("expense", { title: 'Expense View', expense, user: req.user});
    } catch (error) {
      res.status(500).send({message: error.message || "Error Occured" });
    }
} 

exports.categoricalExpensesByID = async(req, res) => {
    try {
      let categoryId = req.params.id;
      limitNumber = 10;
      const categoryById = await Expense.find({ 'category': categoryId }).limit(limitNumber).where('user').equals(req.user.email);
      res.render("categories", { title: 'Categoric Expenses', categoryById, user: req.user});
    } catch (error) {
      res.status(500).send({message: error.message || "Error Occured" });
    }
} 

exports.submitExpense = async(req, res) =>{
    const infoErrorsObj = req.flash('infoErrors');
    const infoSubmitObj = req.flash('infoSubmit');
    
    const categories = await Category.find({});
    res.render("submit-expense",{ title: 'Add Expense', categories, infoErrorsObj, infoSubmitObj, user: req.user});
}

exports.submitExpenseOnPost = async(req, res) =>{
    try {
        const AWS = require("aws-sdk");
        // s3 config
        const s3 = new AWS.S3({
          accessKeyId: process.env.AWS_ACCESS_KEY_ID,
          secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY, 
        });

        const file = req.files.image;

        const params = {
          Bucket: process.env.AWS_BUCKET, 
          Key: `expenseImageUpload-${Date.now()}-${file.name}`, 
          Body: file.data,
          ACL: "public-read",
        };
        const data = await s3.upload(params).promise();
        console.log(data.Location); 

        const newExpense = new Expense({
          user: req.user.email,
          name: req.body.name,
          description: req.body.description,
          date: req.body.date,
          to: req.body.to,
          category: req.body.category,
          amount: req.body.amount,
          image: data.Location
        });
        
        await newExpense.save();
        req.flash('infoSubmit', 'Expense has been added.')
        res.redirect('/submitExpense');
      } catch (error) {
        console.log(error)
        req.flash('infoErrors', error);
        res.redirect('/submitExpense');
      }
}

exports.submitCategory = async(req, res) =>{
  const infoErrorsObj = req.flash('infoErrors');
  const infoSubmitObj = req.flash('infoSubmit');

  res.render("submit-category",{ title: 'Add Category', infoErrorsObj, infoSubmitObj, user: req.user});
}

exports.submitCategoryOnPost = async(req, res) =>{
  try {
      const AWS = require("aws-sdk");
      // s3 config
      const s3 = new AWS.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID, // your AWS access id
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY, // your AWS access key
      });

      const file = req.files.image;

      const params = {
        Bucket: process.env.AWS_BUCKET,
        Key: `categoryImageUpload-${file.name}`, 
        Body: file.data,
        ACL: "public-read",
      };
      const data = await s3.upload(params).promise();
      console.log(data.Location); 

      const newCategory = new Category({
        name: req.body.name,
        image: data.Location
      });
      
      await newCategory.save();
      req.flash('infoSubmit', 'Category has been added.')
      res.redirect('/submitCategory');
    } catch (error) {
      console.log(error)
      req.flash('infoErrors', error);
      res.redirect('/submitCategory');
    }
}

exports.searchExpenses = async(req, res) =>{
  try {
    let searchTerm = req.body.searchTerm;
    let expenditure = await Expense.find( { $text: { $search: searchTerm, $diacriticSensitive: true } }).where('user').equals(req.user.email);
    res.render("search", { title: 'Expenses - Search', expenditure, user: req.user});
  } catch (error) {
    res.status(500).send({message: error.message || "Error Occured" });
  }
  
}

exports.chartsExpenses = async(req, res) =>{

  Expense.aggregate([
    {
      $facet: {
        categories: [
          { $match : { user : req.user.email } } ,
          { 
          $group: { 
            _id: "$category", categoricalExpenses: 
            { $sum: "$amount" } 
          } 
        }],
        dates: [
          { $match : { user : req.user.email } } ,
          {
          $group: { 
            _id: { 
              year: { $year: "$date" }, 
              month: { $month: "$date" } 
            }, 
            monthExpenses: 
            { $sum: "$amount" } 
          } 
          },
          { $sort : { 
            '_id.year': 1, 
            '_id.month': 1, 
          }
        },
        ]
      }
    }
])
    .then(result => {

      var dataPresent;
      if(result[0].dates.length > 0){
        dataPresent = true;
      }
      else{
        dataPresent = false;
      }

      var categoryNames = new Array(result[0].categories.length);
      var categoryData = new Array(result[0].categories.length);

      result[0].categories.forEach(categoryNamesToSums)
      function categoryNamesToSums(item, index) {
        categoryData[index] = item.categoricalExpenses;
        categoryNames[index] = item._id;
      }
      var monthYearNames = new Array(result[0].dates.length);
      var monthYearData = new Array(result[0].dates.length);

      result[0].dates.forEach(monthYearNamesToSums)
      function monthYearNamesToSums(item, index) {
        monthYearData[index] = item.monthExpenses;
        monthYearNames[index] = "".concat(item._id.month, "/", item._id.year);
      }
      res.render("charts", { title: 'Expenses - Charts', dataPresent, categoryNames, categoryData, monthYearNames, monthYearData, user: req.user});
    })
    .catch(error => {
        console.log(error)
    })
}

// Admin Operations
exports.adminManages = async(req, res) =>{
  res.render("adminManagement", { title: 'Management', user: req.user});
}