const User = require("../models/User");
const jwt = require('jsonwebtoken');

// create json web token
const maxAge = 3 * 24 * 60 * 60; //3days
const createToken = (id) => {
  return jwt.sign({ id }, 'secret', {
    expiresIn: maxAge
  });
};

// controller actions
module.exports.signupUser_get = (req, res) => {
  const entryType = "Sign Up"
  const errorMsg = req.flash('signUpFailure');
  res.render('entry-point', {entryType, errorMsg, user: req.user, action: 'signupUser'});
}

module.exports.signinUser_get = (req, res) => {
  const entryType = "Sign In"
  const errorMsg = req.flash('signInFailure');
  res.render('entry-point', {entryType, errorMsg, user: req.user, action: 'signinUser'});
}

module.exports.signupUser_post = async (req, res) => {
  const { email, password } = req.body;
  const role = 'User';

  try {
    const user = await User.create({ email, password, role });
    const token = createToken(user._id);
    res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 });
    res.redirect('/')
  }
  catch(err) {
    req.flash('signUpFailure', err.message)
    res.redirect('/signupUser');
  }
}

module.exports.signinUser_post = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.signinUser(email, password);
    const token = createToken(user._id);
    res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 });

    if(user.role == 'Admin'){
      res.redirect('/adminManagement')
    }
    else{
      res.redirect('/')
    }
  } 
  catch (err) {
    req.flash('signInFailure', err.message)
    res.redirect('/signinUser');
  }
}

module.exports.signoutUser_get = (req, res) => {
  res.cookie('jwt', '', { maxAge: 1 });
  res.redirect('/');
}