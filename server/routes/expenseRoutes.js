const express = require('express');
const router = express. Router();
const expenseController = require('../controllers/expenseController');
const { requireAuth, requireAdminAuth } = require('../middleware/authMiddleware');
/*
App Routes
*/

router.get('/', requireAuth, expenseController.homepage);
router.get('/recent-expenses', requireAuth, expenseController.recentExpenses);
router.get('/high-expenses', requireAuth, expenseController.highExpenses);
router.get('/categories', requireAuth, expenseController.categoricalExpenses);
router.get('/categories/:id', requireAuth, expenseController.categoricalExpensesByID);
router.get('/expense/:id', requireAuth, expenseController.viewExpense);
router.get('/submitExpense', requireAuth, expenseController.submitExpense);
router.post('/submitExpense', requireAuth, expenseController.submitExpenseOnPost);
router.post('/search', requireAuth, expenseController.searchExpenses);
router.get('/charts', requireAuth, expenseController.chartsExpenses);

router.get('/adminManagement', requireAdminAuth, expenseController.adminManages);
router.get('/submitCategory', requireAdminAuth, expenseController.submitCategory);
router.post('/submitCategory', requireAdminAuth, expenseController.submitCategoryOnPost);

module.exports = router;