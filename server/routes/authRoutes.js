const { Router } = require('express');
const authController = require('../controllers/authController');
const { requireAuth} = require('../middleware/authMiddleware');

const router = Router();

router.get('/signupUser', authController.signupUser_get);
router.post('/signupUser', authController.signupUser_post);
router.get('/signinUser', authController.signinUser_get);
router.post('/signinUser', authController.signinUser_post);
router.get('/signoutUser', requireAuth, authController.signoutUser_get);

module.exports = router;