const jwt = require('jsonwebtoken');
const User = require('../models/User');

const requireAuth = (req, res, next) => {
  const token = req.cookies.jwt;

  // check json web token exists & is verified
  if (token) {
    jwt.verify(token, 'secret', async(err, decodedToken) => {
      if (err) {
        req.user = null;
        console.log(err.message);
        res.redirect('/signinUser');
      } else {
        let user = await User.findById(decodedToken.id);
        req.user = user;
        next();
      }
    });
  } else {
    req.user = null;
    res.redirect('/signinUser');
  }
};

const requireAdminAuth = (req, res, next) => {
  const token = req.cookies.jwt;

  // check json web token exists & is verified
  if (token) {
    jwt.verify(token, 'secret', async(err, decodedToken) => {
      if (err) {
        req.user = null;
        console.log(err.message);
        res.redirect('/signinUser');
      } else {
        let user = await User.findById(decodedToken.id);
        if(user.role != 'Admin')
        {
          res.redirect('/signinUser')
        }
        req.user = user;
        next();
      }
    });
  } else {
    req.user = null;
    res.redirect('/signinUser');
  }
};

module.exports = { requireAuth , requireAdminAuth};