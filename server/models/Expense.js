const mongoose = require('mongoose');

const expenseSchema = new mongoose.Schema ({
    user:{
        type: String,
        required: [true, 'User field is required'],
    },
    name: {
        type: String,
        required: [true, 'Name field is required'],
    },
    description: {
        type: String
    },
    date: {
        type: Date,
        required: [true, 'Date field is required'],
    },
    to: {
        type: String,
        required: [true, 'This field is required'],
    },
    category: {
        type: String,
        required: [true, 'Category field is required'],
    },
    amount: {
        type: Number,
        required: [true, 'Amount field is required'],
    },
    image: {
        type: String,
        required: [true, 'Image field is required'],
    }
});

expenseSchema.index({ user: 1, name: 'text', description: 'text' });

module.exports = mongoose.model('Expense', expenseSchema);