const mongoose = require('mongoose');
const categorySchema = new mongoose.Schema ({
    name: {
        type: String,
        required: [true, 'Name field is required'],
        unique: true
    },
    image: {
        type: String,
        required: [true, 'Image field is required'],
    }
});

module.exports = mongoose.model('Category', categorySchema);